import Routes from './routes/'
import './css/index.css'

import { AuthProvider } from './context/auth'

function App() {
  return (
    <AuthProvider>
      <Routes />
    </AuthProvider>
  )
}

export default App
