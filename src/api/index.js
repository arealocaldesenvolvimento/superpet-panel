import axios from 'axios'

const http = axios.create({
  baseURL: 'https://superpet.umdigital.com.br'
})

http.interceptors.response.use(function (response) {
  if (response && response.data && response.data.access_token) {
    localStorage.setItem('token', response.data.access_token)
  }
  return response
})

http.interceptors.request.use(async function (request) {
  const access_token = localStorage.getItem('token')
  if (access_token) {
    request.headers.Authorization = `Bearer ${access_token}`
  }
  return request
})

export default http
