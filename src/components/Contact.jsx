import { Panel } from 'primereact/panel'
import { Message } from 'primereact/message'

export const Contact = ({ user }) => {
  const { index } = user
  const { access } = JSON.parse(localStorage.getItem('user'))
  console.log(access)

  return (
    <>
      {index <= 10 ? (
        <>
          <Panel header={user.Nome}>
            <div className="grid">
              <div className="col-12">
                <b>Fantasia : </b> {user.Fantasia}
              </div>
              <div className="col-12">
                <b>CPF : </b> {user.CPF}
              </div>
              <div className="col-12">
                <b>CNPJ : </b> {user.CNPJ}
              </div>
              <div className="col-12">
                <b>Email : </b> {user.Email}
              </div>
              <div className="col-12">
                <b>Fone : </b> {user.Fone}
              </div>
              <div className="col-12">
                <b>Celular : </b> {user.Celular}
              </div>
              <div className="col-12">
                <b>Tipo : </b> {user.Tipo}
              </div>
              <div className="col-12">
                <b>Departamento : </b> {user.Departamento}
              </div>
            </div>
          </Panel>
          <Panel header="Endereço">
            <div className="col-12">
              <b>Logradouro : </b> {user.Logradouro}
            </div>
            <div className="col-12">
              <b>Número : </b> {user.Numero}
            </div>
            <div className="col-12">
              <b>Bairro : </b> {user.Bairro}
            </div>
            <div className="col-12">
              <b>Cidade : </b> {user.Cidade}
            </div>
            <div className="col-12">
              <b>Estado : </b> {user.Estado}
            </div>
          </Panel>
        </>
      ) : (
        <Message severity="info" text="Sem permissão para visualizar..."></Message>
      )}
    </>
  )
}
