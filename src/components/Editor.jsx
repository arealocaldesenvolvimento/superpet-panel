import { Sidebar } from 'primereact/sidebar'

export const Editor = ({ children, ...props }) => {
  return <Sidebar {...props}>{children}</Sidebar>
}
