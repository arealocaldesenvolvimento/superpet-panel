import { Button } from 'primereact/button'

export default ({ ...props }) => <Button {...props} />
