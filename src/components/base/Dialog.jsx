import { Dialog } from 'primereact/dialog'

export default ({ children, ...props }) => <Dialog {...props}>{children}</Dialog>
