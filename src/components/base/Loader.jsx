import { ProgressSpinner } from 'primereact/progressspinner'
import { Container } from '../layout/Container'
export const Loader = () => {
  return (
    <>
      <Container>
        <div className="flex justify-content-center">
          <ProgressSpinner />
        </div>
      </Container>
    </>
  )
}
