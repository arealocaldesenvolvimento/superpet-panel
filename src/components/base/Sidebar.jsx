import { useContext } from 'react'
import { Link } from 'react-router-dom'
import Button from './Button'
import { AuthContext } from '../../context/auth'
import { useNavigate } from 'react-router-dom'

export default () => {
  const navigate = useNavigate()
  const { handleSignOut } = useContext(AuthContext)
  const user = JSON.parse(localStorage.getItem('user'))
  const { type } = user

  const logout = () => {
    handleSignOut()
    navigate('/')
  }

  return (
    <>
      <div className="navbar shadow-1  p-3" style={style.navbar}>
        <div style={style.menuWrapper} className="flex flex-column align-items-start">
          <Link to="/" style={style.menuItem}>
            <Button icon="pi pi-home" label="Dashboard" className="p-button" style={style.link} />
          </Link>

          {type === 'admin' && (
            <Link to="/users" style={style.menuItem}>
              <Button icon="pi pi-user" label="Usuários" className="p-button" style={style.link} />
            </Link>
          )}

          <Link to="/contacts" style={style.menuItem}>
            <Button icon="pi pi-users" label="Contatos" className="p-button" style={style.link} />
          </Link>

          <span style={{ ...style.menuItem, ...style.logout }}>
            <Button
              onClick={logout}
              icon="pi pi-sign-out"
              label="Sair"
              className="p-button logout"
              style={style.link}
            />
          </span>
        </div>
      </div>
    </>
  )
}

const style = {
  navbar: {
    position: 'fixed',
    height: '100vh',
    width: 300
  },

  menuWrapper: {
    position: 'relative',
    height: '100%'
  },

  menuItem: {
    width: '100%',
    textDecoration: 'none'
  },

  link: {
    textAlign: 'center',
    marginTop: 8,
    width: '100%',
    backgroundColor: '#f5f5f5',
    border: 'none',
    padding: 10,
    fontWeight: 'bold',
    color: 'gray',
    borderRadius: 5,
    cursor: 'pointer'
  },

  logout: {
    position: 'absolute',
    bottom: 10
  }
}
