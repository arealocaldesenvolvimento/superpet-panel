export const Container = ({ children }) => (
  <div className="grid align-items-center justify-content-center h-screen">
    <div className="col-9 xl:col-4 lg:col-6 md:col-9 sm:col-12">{children}</div>
  </div>
)
