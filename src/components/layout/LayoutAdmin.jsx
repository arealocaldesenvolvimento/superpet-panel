import Sidebar from '../base/Sidebar'
export const LayoutAdmin = ({ children }) => {
  return (
    <div className="grid">
      <div className="col-3 p-0" style={style.sidebar}>
        <Sidebar />
      </div>
      <div className="col-9">{children}</div>
    </div>
  )
}

const style = {
  sidebar: {
    maxWidth: 320
  }
}
