import { createContext, useState, useCallback, useLayoutEffect } from 'react'
import { Loader } from '../components/base/Loader'

import http from '../api/'

export const AuthContext = createContext()

export const AuthProvider = ({ children }) => {
  const [signed, setSigned] = useState(false)
  const [bootstraped, setBootstraped] = useState(true)

  function handleSignIn(data) {
    const { token } = data
    const { user } = data

    localStorage.setItem('token', token)
    localStorage.setItem('user', JSON.stringify(user))

    setSigned(true)
  }

  function handleSignOut() {
    localStorage.clear()
    setSigned(false)
  }

  const bootstrap = useCallback(() => {
    http
      .get('/auth')
      .then((response) => {
        localStorage.setItem('token', response.data.token)
        localStorage.setItem('user', JSON.stringify(response.data.user))
        setSigned(true)
        setBootstraped(false)
      })
      .catch((_) => {
        localStorage.clear('user')
        localStorage.clear('token')

        setSigned(false)
        setBootstraped(false)
      })
  }, [])

  useLayoutEffect(() => {
    bootstrap()
  }, [bootstrap])

  if (bootstraped) {
    return <Loader />
  }

  return <AuthContext.Provider value={{ signed, handleSignIn, handleSignOut }}>{children}</AuthContext.Provider>
}
