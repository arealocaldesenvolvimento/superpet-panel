import React from 'react'

import Auth from '../view/auth'

const AuthRoutes = () => {
  return <Auth />
}

export default AuthRoutes
