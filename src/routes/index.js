import { useContext, useMemo } from 'react'

import AuthRoutes from './auth.routes'
import AppRoutes from './app.routes'

import { AuthContext } from '../context/auth'

const Routes = () => {
  const { signed } = useContext(AuthContext)

  const Component = useMemo(() => {
    if (!signed) {
      return AuthRoutes
    }
    return AppRoutes
  }, [signed])

  return <Component />
}

export default Routes
