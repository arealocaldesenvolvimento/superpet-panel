import React, { useState, useEffect, useCallback, useRef } from 'react'

import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Toast } from 'primereact/toast'
import { Button } from 'primereact/button'
import { Toolbar } from 'primereact/toolbar'
import { InputText } from 'primereact/inputtext'
import { Editor } from '../../../components/Editor'
import { Contact } from '../../../components/Contact'
import '../../../css/DataTable.css'
import http from '../../../api/'

export default () => {
  const [users, setUsers] = useState([])
  const [user, setUser] = useState([])
  const [globalFilter, setGlobalFilter] = useState(null)
  const [loading, setLoading] = useState(false)
  const [dialog, setDialog] = useState(false)
  const toast = useRef(null)
  const dt = useRef(null)
  const { access } = JSON.parse(localStorage.getItem('user'))

  const fetchUsers = useCallback(async () => {
    setLoading(true)
    const user = JSON.parse(localStorage.getItem('user'))
    const { nr_inscricao } = user

    try {
      const response = await http.get(`/contacts/inscricao/${nr_inscricao}`)
      const { data } = response

      let i = 0
      let contacts = []
      for (const key in data) {
        if (data[key].length > 0) {
          const info = data[key]
          for (const key in info) {
            i++
            info[key]['index'] = i
            contacts = [...contacts, info[key]]
          }
        }
      }
      setUsers(contacts)
      setLoading(false)
    } catch (error) {}
  }, [])

  useEffect(() => {
    fetchUsers()
  }, [fetchUsers])

  const editProduct = (user) => {
    setUser({ ...user })
    setDialog(true)
  }

  const exportCSV = () => {
    dt.current.exportCSV()
  }

  const header = (
    <div className="table-header">
      <h5 className="mx-0 my-1">Lista de usuários</h5>
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText type="text" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Procurar..." />
      </span>
    </div>
  )

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button label="Exportar" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
      </React.Fragment>
    )
  }

  const actionBodyTemplate = (rowData) => {
    const { index } = rowData
    return (
      <React.Fragment>
        {index <= 10 || access ? (
          <Button
            icon="pi pi-id-card"
            className="p-button-rounded p-button-success mr-2"
            onClick={() => editProduct(rowData)}
          />
        ) : (
          <Button disabled icon="pi pi-lock" className="p-button-rounded p-button-info mr-2" />
        )}
      </React.Fragment>
    )
  }

  const renderTable = () => {
    return (
      <>
        <div className="datatable-crud-demo">
          <Toast ref={toast} />
          <div className="card" style={{ overflow: 'auto' }}>
            <Toolbar className="mb-4" right={rightToolbarTemplate}></Toolbar>

            <DataTable
              loading={loading}
              ref={dt}
              value={users}
              dataKey="id"
              paginator
              rows={10}
              rowsPerPageOptions={[5, 10, 25]}
              paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
              currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
              globalFilter={globalFilter}
              header={header}
              responsiveLayout="scroll"
            >
              <Column headerStyle={{ width: '3rem' }} exportable={false}></Column>
              <Column field="Nome" header="Nome" sortable style={{ minWidth: '10rem' }}></Column>
              <Column field="Email" header="E-mail" sortable style={{ minWidth: '10rem' }}></Column>
              <Column field="Tipo" header="Tipo" sortable style={{ minWidth: '10rem' }}></Column>
              <Column field="Fantasia" header="Fantasia" sortable style={{ minWidth: '10rem' }}></Column>
              <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
            </DataTable>
          </div>
        </div>

        <Editor
          visible={dialog}
          position="right"
          style={{ width: '450px' }}
          header="Product Details"
          modal
          className="p-fluid"
          onHide={() => setDialog(false)}
        >
          <div className="p-dialog-content">{user !== '' && <Contact user={user} />}</div>
        </Editor>
      </>
    )
  }

  return (
    <>
      {users != '' ? (
        renderTable()
      ) : (
        <div>
          <h1>Sem dados para exibição</h1>
        </div>
      )}
    </>
  )
}
