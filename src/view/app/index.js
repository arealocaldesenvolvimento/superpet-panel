import React, { lazy, Suspense } from 'react'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Loader } from '../../components/base/Loader'
import { LayoutAdmin } from '../../components/layout/LayoutAdmin'

const Dashboard = lazy(() => import('./dashboard/'))
const Users = lazy(() => import('./users/'))
const Contacts = lazy(() => import('./contacts/'))

export default () => {
  const user = JSON.parse(localStorage.getItem('user'))
  const { type } = user

  return (
    <>
      <Router>
        <LayoutAdmin>
          <Routes>
            <Route
              exact
              path="/"
              element={
                <Suspense fallback={<Loader />}>
                  <Dashboard />
                </Suspense>
              }
            />
            {type === 'admin' && (
              <Route
                exact
                path="/users"
                element={
                  <Suspense fallback={<Loader />}>
                    <Users />
                  </Suspense>
                }
              />
            )}

            <Route
              exact
              path="/contacts"
              element={
                <Suspense fallback={<Loader />}>
                  <Contacts />
                </Suspense>
              }
            />
          </Routes>
        </LayoutAdmin>
      </Router>
    </>
  )
}
