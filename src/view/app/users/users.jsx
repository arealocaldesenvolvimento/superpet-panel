import React, { useState, useEffect, useCallback, useRef } from 'react'

import { DataTable } from 'primereact/datatable'
import { Column } from 'primereact/column'
import { Toast } from 'primereact/toast'
import { Button } from 'primereact/button'
import { Toolbar } from 'primereact/toolbar'
import { RadioButton } from 'primereact/radiobutton'
import { Editor } from '../../../components/Editor'
import { InputText } from 'primereact/inputtext'
import '../../../css/DataTable.css'
import http from '../../../api/'
import moment from 'moment'

export default () => {
  const [users, setUsers] = useState(null)
  const [user, setUser] = useState([])
  const [loading, setLoading] = useState(false)
  const [isUpdated, setUpdated] = useState(false)
  const [globalFilter, setGlobalFilter] = useState(null)
  const [dialog, setDialog] = useState(false)
  const toast = useRef(null)
  const dt = useRef(null)

  const updateUser = async () => {
    const response = await http.put(`/users/${user._id}`, {
      ...user
    })
    toast.current.show({ severity: 'success', summary: 'Successful', detail: 'Cadastro atualizado', life: 3000 })
    setUpdated(true)
    setDialog(false)
  }

  const fetchUsers = useCallback(async () => {
    setLoading(true)
    const response = await http.get('/users')
    setUsers(response.data)
    setLoading(false)
  }, [])

  const onAccessChange = (e) => {
    let _user = { ...user }
    _user['access'] = e.value == null ? true : !e.value
    setUser(_user)
  }

  const onInputChange = (e, name) => {
    const val = (e.target && e.target.value) || ''
    let _user = { ...user }
    _user[`${name}`] = val

    setUser(_user)
  }

  useEffect(() => {
    fetchUsers()
  }, [isUpdated, fetchUsers])

  const editProduct = (user) => {
    setUser({ ...user })
    setDialog(true)
  }

  const exportCSV = () => {
    dt.current.exportCSV()
  }

  const header = (
    <div className="table-header">
      <h5 className="mx-0 my-1">Lista de usuários</h5>
      <span className="p-input-icon-left">
        <i className="pi pi-search" />
        <InputText type="text" onInput={(e) => setGlobalFilter(e.target.value)} placeholder="Procurar..." />
      </span>
    </div>
  )

  const ContactsBodyTemplate = ({ nr_inscricao }) => {
    if (nr_inscricao !== undefined && nr_inscricao !== '') {
      return nr_inscricao.length
    }

    return 0
  }

  const formatDate = ({ createdAt }) => {
    return moment(createdAt).format('DD/MM/YYYY')
  }

  const rightToolbarTemplate = () => {
    return (
      <React.Fragment>
        <Button label="Exportar" icon="pi pi-upload" className="p-button-help" onClick={exportCSV} />
      </React.Fragment>
    )
  }

  const actionBodyTemplate = (rowData) => {
    return (
      <React.Fragment>
        <Button
          icon="pi pi-pencil"
          className="p-button-rounded p-button-success mr-2"
          onClick={() => editProduct(rowData)}
        />
      </React.Fragment>
    )
  }

  return (
    <>
      <div className="datatable-crud-demo">
        <Toast ref={toast} />

        <div className="card" style={{ overflow: 'auto' }}>
          <Toolbar className="mb-4" right={rightToolbarTemplate}></Toolbar>

          <DataTable
            loading={loading}
            ref={dt}
            value={users}
            dataKey="id"
            paginator
            rows={10}
            rowsPerPageOptions={[5, 10, 25]}
            paginatorTemplate="FirstPageLink PrevPageLink PageLinks NextPageLink LastPageLink CurrentPageReport RowsPerPageDropdown"
            currentPageReportTemplate="Showing {first} to {last} of {totalRecords} products"
            globalFilter={globalFilter}
            header={header}
            responsiveLayout="scroll"
          >
            <Column headerStyle={{ width: '3rem' }} exportable={false}></Column>
            <Column field="name" header="Nome" sortable style={{ minWidth: '10rem' }}></Column>
            <Column field="email" header="E-mail" sortable style={{ minWidth: '10rem' }}></Column>
            <Column
              field="createdAt"
              header="Data Cadastro"
              body={formatDate}
              sortable
              style={{ minWidth: '10rem' }}
            ></Column>
            <Column
              field="nr_inscricao"
              header="Contatos"
              body={ContactsBodyTemplate}
              sortable
              style={{ minWidth: '10rem' }}
            ></Column>
            <Column body={actionBodyTemplate} exportable={false} style={{ minWidth: '8rem' }}></Column>
          </DataTable>
        </div>
      </div>

      <Editor
        visible={dialog}
        position="right"
        style={{ width: '450px' }}
        header="Product Details"
        modal
        className="p-fluid"
        onHide={() => setDialog(false)}
      >
        <div className="p-dialog-content">
          {user !== '' && (
            <>
              <div className="field">
                <label htmlFor="name">Nome</label>
                <InputText onInput={(e) => onInputChange(e, 'name')} value={user.name} required autoFocus />
              </div>
              <div className="field">
                <label htmlFor="email">E-mail</label>
                <InputText onInput={(e) => onInputChange(e, 'email')} value={user.email} required autoFocus />
              </div>

              <div className="field">
                <div className="formgrid grid m-0">
                  <div className="field-radiobutton col-5">
                    <RadioButton
                      inputId="category1"
                      name="category"
                      value={user.access}
                      onChange={onAccessChange}
                      checked={user.access === true}
                    />
                    <label htmlFor="category1">Liberar acesso</label>
                  </div>
                </div>
              </div>
            </>
          )}

          <div className="p-dialog-footer pb-5">
            <React.Fragment>
              <Button label="Cancelar" icon="pi pi-times" className="p-button-text" />
              <Button label="Salvar" icon="pi pi-check" className="p-button-text" onClick={updateUser} />
            </React.Fragment>
          </div>
        </div>
      </Editor>
    </>
  )
}
