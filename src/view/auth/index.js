import React, { lazy, Suspense } from 'react'

import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { Container } from '../../components/layout/Container'

import { Loader } from '../../components/base/Loader'
const Login = lazy(() => import('./login'))

export default () => {
  return (
    <Container>
      <Router>
        <Suspense fallback={<Loader />}>
          <Routes>
            <Route exact path="/" element={<Login />}></Route>
          </Routes>
        </Suspense>
      </Router>
    </Container>
  )
}
