import { useState, useContext, useRef } from 'react'
import { Panel } from 'primereact/panel'
import { InputText } from 'primereact/inputtext'
import { Password } from 'primereact/password'
import { Toast } from 'primereact/toast'
import Button from '../../../components/base/Button'
import http from '../../../api/'

import { AuthContext } from '../../../context/auth'

export default () => {
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState([])
  const toast = useRef(null)

  const { handleSignIn } = useContext(AuthContext)

  const handleData = ({ target: { name, value } }) => {
    setData({ ...data, [name]: value })
  }

  const login = () => {
    setLoading(true)
    http
      .post('/auth', { ...data })
      .then((response) => {
        handleSignIn(response.data)
      })
      .catch(() => {
        setLoading(false)
        toast.current.show({ severity: 'error', summary: 'Erro', detail: 'E-mail ou senha inválidos', life: 3000 })
      })
  }

  return (
    <>
      <Toast ref={toast} />
      <Panel header="Login">
        <div className="grid p-fluid align-items-center flex-column">
          <div className="col-8 ">
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i className="pi pi-user"></i>
              </span>
              <InputText name="email" placeholder="E-mail" onInput={handleData} />
            </div>
          </div>
          <div className="col-8">
            <div className="p-inputgroup">
              <span className="p-inputgroup-addon">
                <i className="pi pi-lock"></i>
              </span>
              <Password name="password" placeholder="Senha" onInput={handleData} />
            </div>
          </div>
          <div className="col-8">
            <div className="p-inputgroup flex-row justify-content-center">
              <Button onClick={login} label="Enviar" loading={loading} loadingIcon="pi pi-spin pi-spinner" />
            </div>
          </div>
        </div>
      </Panel>
    </>
  )
}
